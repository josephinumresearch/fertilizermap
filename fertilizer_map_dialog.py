# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FertilizerMapDialog
                                 A QGIS plugin
 Creates fertilizer map
                             -------------------
        begin                : 2018-07-24
        git sha              : $Format:%H$
        copyright            : (C) 2018 by GIS-ELA
        email                : lukas.hauer@josephinum.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import unicodedata
from PyQt4 import QtGui, uic
from PyQt4.QtGui import QAction, QIcon, QFileDialog
from qgis.core import QgsVectorLayer, QgsVectorFileWriter


FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'fertilizer_map_dialog_base.ui'))


class FertilizerMapDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(FertilizerMapDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

    def selectFileFeldgrenzen(self):
        userprofileDownload = os.path.expanduser('~/Downloads')
        options = QFileDialog.Options()
        filename = QFileDialog.getOpenFileName(self, 'Select Feldgrenzen', userprofileDownload, 'Shape Files (*.shp)', options=options)
        self.lineEdit_Feldgrenzen.setText(filename)

        #Set List View with Attributes
        tmp = QgsVectorLayer(filename, "FeldgrenzenLayer", "ogr")
        features = tmp.getFeatures()
        #featureList = [ i.attributes() for i in tmp.getFeatures()]
        model = QtGui.QStandardItemModel()
        self.listView_Attributes.setModel(model)

        for i in features:
            ownFeatureBuilder = i.attributes()

            try:
                tmp =  str(i.id()) +", "+ unicodedata.normalize("NFKD", ownFeatureBuilder[5]).encode("ascii", "ignore") +", "+ str(ownFeatureBuilder[7])
            except (TypeError, IndexError):
                tmp = str(i.id()) +", "+ str(i.attributes())
            item = QtGui.QStandardItem(tmp)
            model.appendRow(item)


#    def selectFileSatBand4(self):
#        userprofileDownload = os.path.expanduser('~/Downloads')
#        options = QFileDialog.Options()
#        filename = QFileDialog.getOpenFileName(self, 'Select Band 4', userprofileDownload, 'Sentinel Files (*)', options=options)
#        self.lineEdit_SatBand4.setText(filename)
#        return filename

#    def selectFileSatBand5(self):
#        userprofileDownload = os.path.expanduser('~/Downloads')
#        options = QFileDialog.Options()
#        filename = QFileDialog.getOpenFileName(self, 'Select Band 5', userprofileDownload, 'Sentinel Files (*)', options=options)
#        self.lineEdit_SatBand5.setText(filename)
#        return filename

#    def selectFileSatBand6(self):
#        userprofileDownload = os.path.expanduser('~/Downloads')
#        options = QFileDialog.Options()
#        filename = QFileDialog.getOpenFileName(self, 'Select Band 6', userprofileDownload, 'Sentinel Files (*)', options=options)
#        self.lineEdit_SatBand6.setText(filename)
#        return filename

#    def selectFileSatBand7(self):
#        userprofileDownload = os.path.expanduser('~/Downloads')
#        options = QFileDialog.Options()
#        filename = QFileDialog.getOpenFileName(self, 'Select Band 7', userprofileDownload, 'Sentinel Files (*)', options=options)
#        self.lineEdit_SatBand7.setText(filename)
#        return filename

#    def selectFileSatBand8(self):
#        userprofileDownload = os.path.expanduser('~/Downloads')
#        options = QFileDialog.Options()
#        filename = QFileDialog.getOpenFileName(self, 'Select Band 8', userprofileDownload, 'Sentinel Files (*)', options=options)
#        self.lineEdit_SatBand8.setText(filename)
#        return filename

    def selectFileSatArchiv(self):
        userprofileDownload = os.path.expanduser('~/Downloads')
        options = QFileDialog.Options()
        filename = QFileDialog.getOpenFileName(self, 'Select Archiv File', userprofileDownload, 'Sentinel Files (*)', options=options)
        self.lineEdit_SatArchiv.setText(filename)
        return filename
