<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_AT" sourcelanguage="en">
<context>
    <name>FertilizerMap</name>
    <message>
        <location filename="../fertilizer_map.py" line="196"/>
        <source>&amp;GISELA Fertilizer</source>
        <translation>&amp;GISELA Fertilizer</translation>
    </message>
    <message>
        <location filename="../fertilizer_map.py" line="187"/>
        <source>GISELA Fertilizer Map</source>
        <translation>GISELA Fertilizer Map</translation>
    </message>
    <message>
        <location filename="../fertilizer_map.py" line="353"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../fertilizer_map.py" line="340"/>
        <source>No correct satellite image archive found</source>
        <translation>Es wurde kein korrektes Satellitenbildarchiv gefunden</translation>
    </message>
    <message>
        <location filename="../fertilizer_map.py" line="353"/>
        <source>No archive found</source>
        <translation>Kein Archiv gefunden</translation>
    </message>
</context>
<context>
    <name>FertilizerMapDialogBase</name>
    <message>
        <location filename="../fertilizer_map_dialog_base.ui" line="14"/>
        <source>GISELA Fertilizer</source>
        <translation>GISELA Fertilizer</translation>
    </message>
    <message>
        <location filename="../fertilizer_map_dialog_base.ui" line="42"/>
        <source>Selection field boundaries</source>
        <translation>Auswahl Feldgrenzen</translation>
    </message>
    <message>
        <location filename="../fertilizer_map_dialog_base.ui" line="55"/>
        <source>Field boundaries</source>
        <translation>Feldgrenzen</translation>
    </message>
    <message>
        <location filename="../fertilizer_map_dialog_base.ui" line="101"/>
        <source>Archive selection</source>
        <translation>Archiv auswahl</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../fertilizer_map_widget.ui" line="14"/>
        <source>GIS-ELA Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fertilizer_map_widget.ui" line="21"/>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fertilizer_map_widget.ui" line="26"/>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
