# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FertilizerMap
                                 A QGIS plugin
 Creates fertilizer map
                              -------------------
        begin                : 2018-07-24
        git sha              : $Format:%H$
        copyright            : (C) 2018 by GIS-ELA
        email                : lukas.hauer@josephinum.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# Initialize Qt resources from file resources.py
import processing
from qgis.analysis import QgsRasterCalculator, QgsRasterCalculatorEntry
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QFileDialog, QColor, QMessageBox
from qgis.PyQt.QtCore import QVariant
from qgis.PyQt.QtGui import QColor
from qgis.core import *
# Import the code for the dialog
from fertilizer_map_dialog import FertilizerMapDialog
import os.path, sys, zipfile, shutil, datetime, time
from .resources import *


class FertilizerMap:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'FertilizerMap_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        #self.dlg = FertilizerMapDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&GISELA Fertilizer')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'FertilizerMap')
        self.toolbar.setObjectName(u'FertilizerMap')

#        self.dlg.lineEdit_Feldgrenzen.clear()
#        self.dlg.pushButton_Feldgrenzen.clicked.connect(selectFileFeldgrenzen)

#        self.dlg.lineEdit_SatBand4.clear()
#        self.dlg.pushButton_SatBand4.clicked.connect(self.selectFileSatBand4)

#        self.dlg.lineEdit_Satband8.clear(self)
#        self.dlg.pushButton_SatBand8.clicked.connect(self.selectFileSatBand8)


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('FertilizerMap', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = FertilizerMapDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/FertilizerMap/icon.png'
#        icon_path = ':/plugins/FertilizerMap/traktor.png'
        self.add_action(
            icon_path,
            text=self.tr(u'GISELA Fertilizer Map'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&GISELA Fertilizer'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    def extractArchiv(self, pathArchiv):
        newDir = str(pathArchiv.rsplit("/",1)[0]) + "/tmp-"
        newDir = newDir + str(datetime.datetime.now().date()) + "/"

        ### Check if directory already exists! ###
        self.isExisting(newDir)

        if zipfile.is_zipfile(pathArchiv):
            band4 = band5 = band6 = band7 = band8 = None
            zf = zipfile.ZipFile(pathArchiv, "r")
            zf.extractall(newDir)
            for name in os.listdir(newDir):
                if "B04" in name:
                    name = newDir + name
                    band4 = self.iface.addRasterLayer(name, "band4")
                elif "B05" in name:
                    name = newDir + name
                    band5 = self.iface.addRasterLayer(name, "band5")
                elif "B06" in name:
                    name = newDir + name
                    band6 = self.iface.addRasterLayer(name, "band6")
                elif "B07" in name:
                    name = newDir + name
                    band7 = self.iface.addRasterLayer(name, "band7")
                elif "B08" in name:
                    name = newDir + name
                    band8 = self.iface.addRasterLayer(name, "band8")
        else:
            print("No Zipfile!")
            QMessageBox.information(None, self.tr('Error'), self.tr('No Zipfile selected!'))

        return band4, band5, band6, band7, band8


    def transformCRS(self, feldgrenzen):
        layerCRS = feldgrenzen.crs().authid()
        layerName = feldgrenzen.name()
        newFileName = feldgrenzen.source().rsplit("/",1)[-1]
        newFileName = newFileName.rsplit(".")[0]
        path = feldgrenzen.source().rsplit("/",1)[0]
        path = path + "/" + newFileName +"_WGS84.shp"

        ### Checks filepath if already there ###
        self.isExisting(path)

        if str(layerCRS) != "EPSG:4326":
            print("Wrong CRS detected start to convert!")

            reproject=processing.runalg('qgis:reprojectlayer', feldgrenzen, 'EPSG:4326', path)
            self.cleanUp(feldgrenzen)
            layerWgs84 = self.iface.addVectorLayer(reproject['OUTPUT'], layerName, "ogr")

            return layerWgs84
        else:
            return feldgrenzen


    def loadLayer(self):

        fullPathFeldgrenzen = self.dlg.lineEdit_Feldgrenzen.text()
#        fullPathBand4 = self.dlg.lineEdit_SatBand4.text()
#        fullPathBand5 = self.dlg.lineEdit_SatBand5.text()
#        fullPathBand6 = self.dlg.lineEdit_SatBand6.text()
#        fullPathBand7 = self.dlg.lineEdit_SatBand7.text()
#        fullPathBand8 = self.dlg.lineEdit_SatBand8.text()
        fullPathBand4 = 0
        fullPathBand5 = 0
        fullPathBand6 = 0
        fullPathBand7 = 0
        fullPathBand8 = 0
        fullPathArchiv = self.dlg.lineEdit_SatArchiv.text()
        ###Get and prepare the selected Attribute###
        selectedAttribute = self.dlg.listView_Attributes.selectedIndexes()

        ### Error Check ###
        if not os.path.exists(fullPathFeldgrenzen):
            print("##########################################################")
            print("Die eAMA Feldgrenzen werden ebenfalls benoetigt!")
            QMessageBox.information(None, self.tr('Error'), self.tr('No field boundaries found'))
            return 1, 1, 1, 1, 1, 1

        else:
            feldgrenzen = self.iface.addVectorLayer(fullPathFeldgrenzen, 'feldgrenzen', 'ogr')
            feldgrenzen = self.transformCRS(feldgrenzen)
            ### Added originalFeldgrenzen - allow us to remove the hole field afterwards ###
            originalFeldgrenzen = feldgrenzen

            print("Original Feldgrenzen Pfad :" +str(originalFeldgrenzen.source()))

            if selectedAttribute:
                print("Selected Attribute: " +str(selectedAttribute[0].data()).split(",")[0])
                selectedFeatureID = str(selectedAttribute[0].data()).split(",")[0]
                feldgrenzenFeatures = feldgrenzen.getFeatures()

                for f in feldgrenzenFeatures:
                    fID = f.id()
                    if int(selectedFeatureID) == int(fID):
                        selectedFeature = f

                feldgrenzen.setSelectedFeatures([selectedFeature.id()])
                ### Working ###
                path = feldgrenzen.source().rsplit("/",1)[0] + "/AusgewaehltesFeldstueck.shp"
                self.isExisting(path)
                selectedArea = processing.runalg("qgis:multiparttosingleparts", feldgrenzen, path)

                feldgrenzen = self.iface.addVectorLayer(selectedArea["OUTPUT"], "", "ogr")

                if fullPathArchiv:
                    print("Archiv found!")
                    if not os.path.exists(fullPathArchiv):
                        QMessageBox.information(None, self.tr('Error'),
                                                self.tr('No correct satellite image archive found'))
                        print("##########################################################")
                        print("Kein korrektes Satellitenbilder Archiv ausgewaehlt!")
                        return 1, 1, 1, 1, 1, 1

                    else:
                        band4, band5, band6, band7, band8 = self.extractArchiv(fullPathArchiv)
                        self.cleanUp(originalFeldgrenzen)
                        return feldgrenzen, band4, band5, band6, band7, band8

                else:
                    QMessageBox.information(None, self.tr('Error'), self.tr('No archive found'))

                    if not(fullPathBand4 and fullPathBand5 and fullPathBand6 and fullPathBand7 and fullPathBand8):
                        print("##########################################################")
                        print("Kein korrekter Pfad zu Satellitenbilder!")
                        return 1, 1, 1, 1, 1, 1

                    else:
                        date = fullPathBand4.split(",")[0].split("/")[-1]
                        band4 = self.iface.addRasterLayer(fullPathBand4, "band4-"+date)
                        band5 = self.iface.addRasterLayer(fullPathBand5, "band5-"+date)
                        band6 = self.iface.addRasterLayer(fullPathBand6, "band6-"+date)
                        band7 = self.iface.addRasterLayer(fullPathBand7, "band7-"+date)
                        band8 = self.iface.addRasterLayer(fullPathBand8, "band8-"+date)
                        self.cleanUp(originalFeldgrenzen)

                        return feldgrenzen, band4, band5, band6, band7, band8

            else:
                feldgrenzen = self.iface.addVectorLayer(fullPathFeldgrenzen, "feldgrenzen", "ogr")

                if fullPathArchiv:
                    print("Archiv found!")

                    if not os.path.exists(fullPathArchiv):
                        QMessageBox.information(None, self.tr('Error'), self.tr('No working Zipfile selected!'))
                        print("##########################################################")
                        print("Kein korrektes Satellitenbilder Archiv ausgewaehlt!")
                        return 1, 1, 1, 1, 1, 1

                    else:
                        band4, band5, band6, band7, band8 = self.extractArchiv(fullPathArchiv)
#                        self.cleanUp(originalFeldgrenzen)

                        return feldgrenzen, band4, band5, band6, band7, band8
                else:
                    print("KEIN Archiv found!")

                    if not(fullPathBand4 and fullPathBand5 and fullPathBand6 and fullPathBand7 and fullPathBand8):
                        QMessageBox.information(None, self.tr('Error'), self.tr('Path to Zipfile is not correct!'))
                        print("##########################################################")
                        print("Kein korrekter Pfad zu Satellitenbilder!")
                        return 1, 1, 1, 1, 1, 1

                    else:
                        date = fullPathBand4.split(",")[0].split("/")[-1]
                        band4 = self.iface.addRasterLayer(fullPathBand4, "band4-"+date)
                        band5 = self.iface.addRasterLayer(fullPathBand5, "band5-"+date)
                        band6 = self.iface.addRasterLayer(fullPathBand6, "band6-"+date)
                        band7 = self.iface.addRasterLayer(fullPathBand7, "band7-"+date)
                        band8 = self.iface.addRasterLayer(fullPathBand8, "band8-"+date)
#                        self.cleanUp(originalFeldgrenzen)

                        return feldgrenzen, band4, band5, band6, band7, band8


    def getRoughBoundaries(self, feldgrenzen):
        ext = feldgrenzen.extent()

        xmin = ext.xMinimum()
        xmax = ext.xMaximum()
        ymin = ext.yMinimum()
        ymax = ext.yMaximum()
        width = ext.width()
        height = ext.height()

        xmin = xmin - (width / 10)
        ymin = ymin - (height / 10)
        xmax = xmax + (width / 5)
        ymax = ymax + (height/ 5)

        virtLayer = QgsVectorLayer("Polygon", "rawCut","memory")
        pr = virtLayer.dataProvider()
        poly = QgsFeature()
        points = [QgsPoint(xmin,ymin), QgsPoint(xmax,ymin), QgsPoint(xmax,ymax), QgsPoint(xmin,ymax)]
        poly.setGeometry(QgsGeometry.fromPolygon([points]))
        pr.addFeatures([poly])
        virtExt = virtLayer.extent()
        virtLayer.updateExtents()
        QgsMapLayerRegistry.instance().addMapLayers([virtLayer])

        return virtLayer


    def clipVecLayer(self, feldgrenzen, selectedLayer):
        ### Clip Two Vector Layers ###
        filename = str(selectedLayer.name())
        output = str(os.path.dirname(selectedLayer.source()))
        output = output + "/" + filename + "-clipped.shp"

        processing.runalg("qgis:clip",selectedLayer,feldgrenzen,output)
        tmp = self.iface.addVectorLayer(output, "", "ogr")

        return tmp


    def clipLayer(self, feldgrenzen, selectedLayer):


        ### Working ###
#        unc = str(feldgrenzen.source().rsplit("/",1)[0])
        ### New ###
        if selectedLayer is not None:
            ### Debugging prints ###
            print "In clipLayer"
            print("Feldgrenzen :" +str(feldgrenzen))
            print("Feldgrenzen Source :" +str(feldgrenzen.source()))
            print("UNC Pfad: "+str(feldgrenzen.source().rsplit("/",1)[0]))
            unc = str(selectedLayer.source().rsplit("/",1)[0])

            outputPath = unc+"/"+selectedLayer.name()+"-clipped.tif"

            if os.path.exists(str(outputPath)):
                self.nukeTiff(str(outputPath))
                self.isExisting(str(outputPath))

            tmp = processing.runalg('gdalogr:cliprasterbymasklayer',
                                                               selectedLayer, #Input
                                                               feldgrenzen,   #Mask
                                                               "0",           #No_Data
                                                               True,          #Alpha Band
                                                               True,          #Crop To Cutline
                                                               True,          #Keep Resolution
                                                               5,             #RType
                                                               0,             #Compress
                                                               1,             #JpegCompression
                                                               1,             #ZLevel
                                                               1,             #Predictor
                                                               False,         #Titled
                                                               0,             #BigTiff
                                                               False,         #TFW
                                                               "",            #Extra
                                                               outputPath)    #Output

            tmp = self.iface.addRasterLayer(outputPath, selectedLayer.name()+"-clipped")

            return tmp
        else:
            print("selectedLayer is None")


    ### Swich Visualization Settings of the Layer                       ###
    ### vegetationIndex contain the Indexname like ndvi                 ###
    ### Classes determines how many classes for visualizations are used ###
    def switchLayerSettings(self, vegetationIndex, classes = 5):
        ### Switch Settings ###
        ### Change Settings in RasterLayer ###
        print("Switch Layer Visualization")

        datasetStats = vegetationIndex.dataProvider().bandStatistics(1)
        datasetStatsMax = datasetStats.maximumValue
        datasetStatsMin = datasetStats.minimumValue

        interval = (datasetStatsMax - datasetStatsMin)/(classes)

        class0 = round(datasetStatsMin + interval, 3)
        class1 = round(class0 + interval, 3)
        class2 = round(class1+ interval, 3)
        class3 = round(class2+ interval, 3)
        class4 = round(class3 + interval, 3)

        fcn = QgsColorRampShader()
        fcn.setColorRampType(QgsColorRampShader.DISCRETE)
        lst = [ QgsColorRampShader.ColorRampItem(class0, QColor(247,252,245),str(class0)),
                   QgsColorRampShader.ColorRampItem(class1, QColor(202,234,195),str(class1)),
                   QgsColorRampShader.ColorRampItem(class2, QColor(123,200,124),str(class2)),
                   QgsColorRampShader.ColorRampItem(class3, QColor(42,146,74),str(class3)),
                   QgsColorRampShader.ColorRampItem(class4, QColor(0,68,27),str(class4)) ]
        fcn.setColorRampItemList(lst)

        shader = QgsRasterShader()
        shader.setRasterShaderFunction(fcn)

        renderer = QgsSingleBandPseudoColorRenderer(vegetationIndex.dataProvider(), 1,shader)
        vegetationIndex.setRenderer(renderer)

        vegetationIndex.dataProvider().reloadData()
        vegetationIndex.triggerRepaint()

        classValues = []
        classValues.append(class0)
        classValues.append(class1)
        classValues.append(class2)
        classValues.append(class3)
        classValues.append(class4)

        return vegetationIndex, classValues



    def createApplicationMap(self, layer, classValues, vegetationsIndexName):
        ### Add Duenger Value  ###
        ### To Attribute Table ###
        print("CreateApplicationMap")
        ### Get imageDate ###
        imageDate = (str(layer.name().rsplit("-",1)[0])).split("-",1)[1]
        print("IMAGEDATE")
        print("######")
        print(imageDate)
        print("######")

        ### Default Values###
        defaultFertilizer0 = "80"
        defaultFertilizer1 = "90"
        defaultFertilizer2 = "100"
        defaultFertilizer3 = "110"
        defaultFertilizer4 = "120"


        expression = QgsExpression("CASE WHEN \"value\" < "+str(classValues[0])+" THEN "+defaultFertilizer0+" WHEN \"value\" < "+str(classValues[1])+" THEN "+defaultFertilizer1+" WHEN \"value\" < "+str(classValues[2])+" THEN "+defaultFertilizer2+" WHEN \"value\" < "+str(classValues[3])+" THEN "+defaultFertilizer3+" ELSE "+defaultFertilizer4+" END")
        layer.startEditing()

        res = layer.dataProvider().addAttributes([QgsField("duenger", QVariant.Int)])
        layer.updateFields()

        index = layer.fieldNameIndex("duenger")
        expression.prepare(layer.pendingFields())

        for feature in layer.getFeatures():
            duenger = expression.evaluate(feature)
            layer.changeAttributeValue(feature.id(), index, duenger)

        layer.commitChanges()

        ### Dissolve Layer      ###
        ### Save Applicationmap ###
        print("Dissolve Layer CreateApplicationMap")
        unc = str(layer.source().rsplit("/",1)[0])
        outputName = unc + "/DUENGEKARTE-"+vegetationsIndexName+"-"+imageDate+".shp"
        outputQgisName = vegetationsIndexName+"-dissolved"

        ### If file already exists - get ###
        ### newly created features and   ###
        ### add them to the exisiting    ###
        ### Link: https://gis.stackexchange.com/questions/267440/creating-qgis-copy-paste-features-between-two-layers-using-pyqgis                                         ###
        ### To copy all features; create a list, get features from source layer, create data provider on destination layer, and finally add features to destination layer: ###
        print("OUTPUTNAME")
        print("######")
        print(outputName)
        print("######")
        if os.path.exists(outputName):
            now = datetime.datetime.now().time()
            nowTime = str(now).split(".")[0].replace(":","-")
            outputName = str(outputName.rsplit("/",1)[0])
            outputName = outputName + "/DUENGEKARTE-"+vegetationsIndexName+"-"+imageDate+ "_" + nowTime + ".shp"

            processing.runandload("qgis:dissolve",layer.source(), False, "", outputName)

        else:
            processing.runandload("qgis:dissolve",layer.source(), False, "", outputName)
        ### Output  ###
#        self.iface.addVectorLayer(outputName, "", "ogr")

        return 0


    def calculateNdvi(self, band4, band8, feldgrenzen):
        print("Calculte NDVI")
        destPath = feldgrenzen.source()

        band4RasterEntry = QgsRasterCalculatorEntry()
        band4RasterEntry.raster = band4
        band4RasterEntry.ref = str(band4.name()+"@1")
        band4RasterEntry.bandNumber = 1

        band8RasterEntry = QgsRasterCalculatorEntry()
        band8RasterEntry.raster = band8
        band8RasterEntry.ref = str(band8.name()+"@1")
        band8RasterEntry.bandNumber = 1

        rasterEntries = [band4RasterEntry, band8RasterEntry]
        #Removes .tif from filepath
        imageDate = (str(band4.name().split("-",1)[-1].rsplit("-",1)[0]))
        ### Working ###
#        unc = str(band4.source().rsplit("/",1)[0])
        ### New ###
        unc = str(destPath.rsplit("/",1)[0])
        unc = unc + "/duengekarte-ndvi-"+ imageDate + "/"

        if os.path.exists(unc):
            now = datetime.datetime.now().time()
            nowMicro = str(now.microsecond)

            outputFile = unc + "ndvi"+imageDate+"-" + nowMicro + ".tif"

        else:
            print("Pfad nicht vorhanden. Erstelle Ordner!")
            os.mkdir(unc)
            outputFile = unc + "ndvi"+imageDate+".tif"

        calculation = "'(\""+band8RasterEntry.ref+"\"-\""+band4RasterEntry.ref+"\")/(\""+band8RasterEntry.ref+"\"+\""+band4RasterEntry.ref+"\")'"
        calc = QgsRasterCalculator(calculation,
                                   outputFile,
                                   "GTiff",
                                   band4.extent(),
                                   band4.width(),
                                   band4.height(),
                                   rasterEntries)
        print("NDVI result: "+str(calc.processCalculation()))
        ndvi = self.iface.addRasterLayer(outputFile, "ndvi-"+imageDate)

        ### Convert to Vector ###
        ### Raster to Vector ###
        extent = ndvi.extent()
        xmin = extent.xMinimum()
        xmax = extent.xMaximum()
        ymin = extent.yMinimum()
        ymax = extent.yMaximum()

        unc = str(ndvi.source().rsplit("/",1)[0])
        ### Added imageDate - allows to add the satelite picture date to the ndvi filename / layername
        outputNdviVector = unc + "/ndviVector-"+imageDate+".shp"
        self.isExisting(outputNdviVector)
        print("Start converting raster to vector!")
        processing.runalg("grass7:r.to.vect", ndvi, 2, True, "%f,%f,%f,%f"% (xmin, xmax, ymin, ymax), 3, outputNdviVector)
        print("Finished converting raster to vector!")
        ### Empty "" because it takes filename as layer name in layer panel ###
        ndviVector = self.iface.addVectorLayer(outputNdviVector, "", "ogr")

        ### Clip the Vectorized NDVI ###
        ndviVectorClipped = self.clipVecLayer(feldgrenzen, ndviVector)
        ndvi = self.clipLayer(feldgrenzen, ndvi)
        ndvi, classValues = self.switchLayerSettings(ndvi)

        ### Create Application Map for NDVI ###
        ### Fix Layer Visualization ###
        self.createApplicationMap(ndviVectorClipped, classValues, "Ndvi")

        ### Remove Original Whole Boundaries ###
        ### Vector Layer ###
        self.cleanUp(ndviVector)
        self.cleanUp(ndviVectorClipped)
        self.nukeLayer(outputNdviVector)
        self.nukeTiff(outputFile)
        self.isExisting(outputFile)

        return ndvi


    ### Calculation for REIP Vegetation Index ###
    def calculateReip(self, band4, band5, band6, band7, feldgrenzen):
        print("Calculte REIP")
        destPath = feldgrenzen.source()

        band4RasterEntry = QgsRasterCalculatorEntry()
        band4RasterEntry.raster = band4
        band4RasterEntry.ref = str(band4.name()+"@1")
        band4RasterEntry.bandNumber = 1
        band4Raster = QgsRasterLayer(band4.source())

        band5RasterEntry = QgsRasterCalculatorEntry()
        band5RasterEntry.raster = band5
        band5RasterEntry.ref = str(band5.name()+"@1")
        band5RasterEntry.bandNumber = 1

        band6RasterEntry = QgsRasterCalculatorEntry()
        band6RasterEntry.raster = band6
        band6RasterEntry.ref = str(band6.name()+"@1")
        band6RasterEntry.bandNumber = 1

        band7RasterEntry = QgsRasterCalculatorEntry()
        band7RasterEntry.raster = band7
        band7RasterEntry.ref = str(band7.name()+"@1")
        band7RasterEntry.bandNumber = 1

        ### RASTERCALCULATOR REIP ###
        rasterEntries = [band4RasterEntry, band5RasterEntry, band6RasterEntry, band7RasterEntry]
        imageDate = (str(band4.name().split("-",1)[-1].rsplit("-",1)[0]))
        indexName = "/reip-"
        ### Working ###
#        unc = str(band4.source().rsplit("/",1)[0])
        ### New ###
        unc = str(destPath.rsplit("/",1)[0])
        unc = unc + "/duengekarte-reip-"+ imageDate + "/"

        if os.path.exists(unc):
            now = datetime.datetime.now().time()
            nowMicro = str(now.microsecond)
            outputFile = unc + "reip"+imageDate+"-" + nowMicro + ".tif"

        else:
            print("Pfad nicht vorhanden. Erstelle Ordner!")
            os.mkdir(unc)
            outputFile = unc + "reip"+imageDate+".tif"

        outputFile = unc + indexName + imageDate+".tif"
        calculation = "'700+40*((((\""+band4RasterEntry.ref+"\"+\""+band7RasterEntry.ref+"\")/2) -\""+band5RasterEntry.ref+"\")/(\""+band6RasterEntry.ref+"\"-\""+band5RasterEntry.ref+"\"))'"
#        print calculation
        calc = QgsRasterCalculator(calculation,
                                   outputFile,
                                   "GTiff",
                                   band4.extent(),
                                   band4.width(),
                                   band4.height(),
                                   rasterEntries)
        calc.processCalculation()
        reip = self.iface.addRasterLayer(outputFile, "reip-"+imageDate)
        ### END RASTERCALCULATOR REIP ###

        ### Convert to Vector ###
        ### Raster to Vector ###
        extent = reip.extent()
        xmin = extent.xMinimum()
        xmax = extent.xMaximum()
        ymin = extent.yMinimum()
        ymax = extent.yMaximum()

        unc = str(reip.source().rsplit("/",1)[0])
        outputReipVector = unc + "/reipVector-"+imageDate+".shp"
        self.isExisting(outputReipVector)
        processing.runalg("grass7:r.to.vect", reip, 2, True, "%f,%f,%f,%f"% (xmin, xmax, ymin, ymax), 3, outputReipVector)
        ### Empty "" because it takes filename as layer name in layer panel ###
        reipVector = self.iface.addVectorLayer(outputReipVector, "", "ogr")

        ### Clip the Vectorized NDVI ###
        reipVectorClipped = self.clipVecLayer(feldgrenzen, reipVector)
        reip = self.clipLayer(feldgrenzen, reip)
        reip, classValues = self.switchLayerSettings(reip)

        ### Create Application Map for REIP ###
        ### Fix Layer Visualization ###
        self.createApplicationMap(reipVectorClipped, classValues, "Reip")

        ### Remove Original Whole Boundaries ###
        ### Vector Layer ###
        self.cleanUp(reipVector)
        self.cleanUp(reipVectorClipped)
        self.nukeLayer(outputReipVector)
        self.nukeTiff(outputFile)
        self.isExisting(outputFile)
        return reip


    ### Check if file exists ###
    def isExisting(self, path):
        time.sleep(1)
        try:
            if os.path.isdir(path):
                print("Directory exists - clean up old directory!")
                shutil.rmtree(path)
            elif os.path.exists(path):
                print("File exists - clean up old file!")
                os.remove(path)
            else:
                print("No file or directory is present - create new one!")
        except WindowsError:
            pass


    ### Cleanup Method ###
    def cleanUp(self, layer):
        if layer:
            time.sleep(0.5)
            layerId = str(layer.id())
            layerName = str(layer.source())
            QgsMapLayerRegistry.instance().removeMapLayer(layerId)
        else:
            print("Layer don't exist")


    def cleanStart(self):
        for layer in QgsMapLayerRegistry.instance().mapLayers().values():
            QgsMapLayerRegistry.instance().removeMapLayer(layer.id())


    def nukeTiff(self, path):
        for layer in QgsMapLayerRegistry.instance().mapLayers().values():
            if path in layer.source():
                QgsMapLayerRegistry.instance().removeMapLayer(layer.id())

        self.iface.mapCanvas().clearCache()


    def nukeLayer(self, path):
        self.iface.mapCanvas().clearCache()
        tmp = str(path.rsplit(".")[0])
        self.isExisting(path)
        self.isExisting(tmp+".dbf")
        self.isExisting(tmp+".prj")
        self.isExisting(tmp+".shx")
        self.isExisting(tmp+".qpj")

    ### Main Entry Point ###
    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            self.iface.mapCanvas().clearCache()
            self.iface.mapCanvas().refreshAllLayers()

            self.cleanStart()

            feldgrenzen, band4, band5, band6, band7, band8 = self.loadLayer()
#            ### Debugging prints ###
#            print("Feldgrenzen und Baender geladen!")
#            print("Feldgrenzen und Baender beinhalten Daten!")
#            print("Main Feldgrenzen :" +str(feldgrenzen))
#            print("Main Feldgrenzen Name :" +str(feldgrenzen.name()))
#            print("Main Feldgrenzen Source :" +str(feldgrenzen.source()))
#            print("Main Band4 :" +str(band4))
#            print("Main Band8 :" +str(band8))
#            print("Main Band4 Name:" +str(band4.name()))
#            print("Main Band4 Source:" +str(band4.source()))

            roughField = self.getRoughBoundaries(feldgrenzen)
            band4Clipped = self.clipLayer(roughField, band4)
            band5Clipped = self.clipLayer(roughField, band5)
            band6Clipped = self.clipLayer(roughField, band6)
            band7Clipped = self.clipLayer(roughField, band7)
            band8Clipped = self.clipLayer(roughField, band8)

            if band4Clipped and band8Clipped is not None:
                ndvi = self.calculateNdvi(band4Clipped, band8Clipped, feldgrenzen)

            if band4Clipped and band5Clipped and band6Clipped and band7Clipped is not None:
                reip = self.calculateReip(band4Clipped, band5Clipped, band6Clipped, band7Clipped, feldgrenzen)

            ### Cleanup unused layers from qgis and filesystem ###
            print("Start with cleanup")
            self.iface.mapCanvas().clearCache()
            tmp = str(band4.source().rsplit("/",1)[0])
            self.cleanUp(feldgrenzen)
            self.cleanUp(roughField)
            self.nukeTiff(tmp)
            self.isExisting(tmp)
            self.iface.mapCanvas().clearCache()

            print("Duengekarten erstellt!")
            QMessageBox.information(None, self.tr('Information'), self.tr('Duengekarte erstellt!'))


class vegetationIndex:
    def __init__(self, name):
        self.name = name
